export class Logger {
  private readonly className: string;
  constructor(className: string) {
    this.className = className;
  }

  private generateLogEntry(data: any, error: any = undefined): string {
    const now = new Date();
    const date: string = `[${now.getFullYear()}-${now.getMonth()}-${now.getDay()}]`
    const time: string = `[${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}.${now.getMilliseconds()}]`
    let entry: string = `${date} ${time} [${this.className}] ${data}`;
    if (error)
      entry += `: ${error}`;
    return entry;
  }

  public trace(data: any) {
    console.trace(this.generateLogEntry(data));
  }

  public debug(message: string) {
    console.debug(this.generateLogEntry(message));
  }

  public info(message: string) {
    console.log(this.generateLogEntry(message));
  }

  public warn(message: string, error: any = undefined) {
    console.warn(this.generateLogEntry(message, error));
  }

  public error(message: string, error: any) {
    console.error(this.generateLogEntry(message, error));
  }
}