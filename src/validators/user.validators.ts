import {ValidationError} from "../errors/validation.error";
import {UserRegistrationDTO} from "../dto/incoming/user.dto";
import {UserService} from "../services/user.service";

export class UserRegistrationValidator {

  userService: UserService;

  constructor() {
    this.userService = new UserService();
  }

  public async validate(data: UserRegistrationDTO): Promise<void> {
    if (!data) {
      throw new ValidationError("userData", "missing");
    }
    if (!data.username) {
      throw new ValidationError("username", "missing");
    }
    if (await this.userService.usernameTaken(data.username)) {
      throw new ValidationError("username", "taken");
    }
    if (!data.password) {
      throw new ValidationError("password", "missing");
    }
    if (!data.email) {
      throw new ValidationError("email", "missing");
    }
  }
}