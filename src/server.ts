import http from 'http';
import express, { Express } from 'express';
import morgan from 'morgan';
import routes from "./routes/index.routes";

const router: Express = express();

/**
 * Logging
 */
router.use(morgan('dev'));

/**
 * Parse requests
 */
router.use(express.urlencoded({ extended: false }));

/**
 * Handle json
 */
router.use(express.json());

/**
 * Set access rules
 */
router.use((req, res, next) => {
  // set the CORS policy
  res.header('Access-Control-Allow-Origin', 'inventory.pfadi-zb.ch');
  // set the CORS headers
  res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With,X-Forwarded-For,Content-Type,Accept, Authorization');
  // set the CORS method headers
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET POST');
    return res.status(200).json({});
  }
  next();
});

/**
 * Add routes
 */
router.use('/', routes);

/**
 * Handle 404
 */
router.use((req, res, next) => {
  const error = new Error('not found');
  return res.status(404).json({
    message: error.message
  });
});

/**
 * Start server
 */
const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 2468;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));