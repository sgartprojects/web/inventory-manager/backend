import {MailParams, MailServiceResponse} from "../models/email.models";
import {environment} from "../environment/environment";
import Mailgun from "mailgun.js";
import {Logger} from "../helpers/Logger";

export class DatabaseService {
  private readonly log: Logger;
  constructor() {
    this.log = new Logger("DatabaseService");
  }

}