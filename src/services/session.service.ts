import {PrismaClient, Session, User} from "@prisma/client";
import {environment} from "../environment/environment";
import {Logger} from "../helpers/Logger";
import {SecurityService} from "./security.service";
import {SessionStatus} from "../enums/session.enums";
import {ApiBaseError} from "../errors/apibase.error";

export class SessionService {
  private log: Logger;
  private prisma: PrismaClient;
  private securityService: SecurityService;

  constructor(securityService: SecurityService) {
    this.log = new Logger('SessionService');
    this.prisma = new PrismaClient();
    this.securityService = securityService;
  }
  public async getSessionForUserAndIpHash(user: User, ipHash: string): Promise<Session> {
    const date: Date = new Date();
    const invalidByDate = new Date(date.setDate(date.getDate() + environment.security.session.validForXDays));
    let session: Session | null;
    session = await this.prisma.session.findFirst({
      where: {
        userId: user.id,
        ipHash: ipHash
      }
    });
    this.log.trace({
      message: `Session found for user ${user.username}`,
      session: session
    })
    if (session != null) {
      if (this.checkValidity(session, ipHash, user)) {
        return session;
      } else {
        console.debug(`Invalidating session with id ${session.id}`);
        this.prisma.session.delete({
          where: {
            id: session.id
          }
        });
        session = null;
      }
    }
    if (session == null) {
      const sessionId = await this.securityService.hashString(`${environment.security.session.secret}${ipHash}${user.username}${invalidByDate.getTime()}${this.securityService.generateTemporaryCode(16)}`)
      session = await this.prisma.session.create({
        data: {
          userId: user.id,
          invalidBy: invalidByDate,
          ipHash: ipHash,
          id: sessionId,
          status: SessionStatus.STATUS_EVAL
        }
      })
      return session;
    }
    this.log.error(`Error creating session`, `Session not null after reset`);
    throw new ApiBaseError(500, `Error creating session`, `Session not null after reset`);
  }

  private checkValidity(session: Session, ipHash: string, user: User): boolean {
    this.log.debug('Validating session');
    const now = new Date();
    if (now >= session.invalidBy) {
      this.log.trace({
        message: `Session for user ${user.username} is too old and thus invalid`,
        session: session
      })
      return false;
    }
    if (session.ipHash !== ipHash) {
      this.log.trace({
        message: `IP address hashes do not match, the session is thus invalid`,
        ipHash: ipHash,
        session: session
      })
      return false;
    }
    if (session.userId !== user.id) {
      this.log.trace({
        message: `Userid doesn't match with session, the session is thus invalid`,
        userId: user.id,
        session: session
      })
      return false;
    }
    this.log.trace({
      message: `Session for user ${user.username} is valid`,
      session: session
    })
    return true;
  }
}