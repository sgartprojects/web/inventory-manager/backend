import {environment} from "../environment/environment";
import Mailgun from "mailgun.js";
import {Logger} from "../helpers/Logger";
import {MailParams, MailServiceResponse} from "../interfaces/email.models";

export class EmailService {
  private readonly log: Logger;
  private readonly config: any;
  private readonly mailgun: Mailgun;
  private readonly client: any;
  constructor() {
    this.log = new Logger("EmailService");
    this.config = environment.mail;
    this.mailgun = new Mailgun(require("form-data"));
    this.client = this.mailgun.client({
      //url: this.config.host,
      url: "https://api.eu.mailgun.net",
      username: "api",
      key: this.config.apiKey
    })
  }

  public async sendMail(mailParams: MailParams): Promise<MailServiceResponse> {
    if (this.config.enabled) {
      this.log.info(`Sending email to ${mailParams.receivers}...`);
      const messageData = {
        from: `${this.config.sender.name} <${this.config.sender.address}>`,
        to: mailParams.receivers,
        subject: mailParams.subject,
        html: mailParams.message || await this.getTemplateWithParams(mailParams.template),
      }
      try {
        const res: any = await this.client.messages.create(this.config.domain, messageData)
        this.log.debug(`Email sent successfully: ${res.message}`);
        return{
          status: res.status,
          message: res.message
        };
      } catch (err: any) {
        this.log.error(`Sending email failed with ${err.message}`, err.details || "Unkown Error");
        return {
          status: err.status,
          message: err.message,
          details: err.details
        }
      }
    } else {
      this.log.info(`Sending Emails is disabled. Intended to send the following message to ${mailParams.receivers}: ${mailParams.message}`)
      return {
        status: 200,
        message: "No email sent: Sending disabled"
      }
    }
  }

  private async getTemplateWithParams(templateParams: any) {

  }
}
