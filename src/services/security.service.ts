import {Logger} from "../helpers/Logger";
import {environment} from "../environment/environment";
import {OneTimeCode, PrismaClient, Session, User} from "@prisma/client";
import {ActivationStatus} from "../enums/user.enums";
import {EmailService} from "./email.service";
import {UserService} from "./user.service";
import {Request} from "express";
import {SessionService} from "./session.service";
import {UserServiceResponse} from "../responses/user.response";
import {AuthData} from "../interfaces/security.models";
import {AuthType, MFAType} from "../enums/security.enums";
import {UserLoginDTO} from "../dto/incoming/user.dto";
import {ApiBaseError} from "../errors/apibase.error";
import {SecurityDTO} from "../dto/outgoing/security.dto";
import {SessionStatus} from "../enums/session.enums";
import {SMSService} from "./sms.service";
import {MailParams} from "../interfaces/email.models";

const bcrypt = require("bcrypt");
const speakeasy = require("speakeasy");

export class SecurityService {
  private readonly log: Logger;
  private readonly prisma: PrismaClient;
  private readonly mailService: EmailService;
  private readonly smsService: SMSService;
  private readonly userService: UserService;
  private readonly sessionService: SessionService;

  constructor() {
    this.log = new Logger("SecurityService");
    this.prisma = new PrismaClient();
    this.mailService = new EmailService();
    this.smsService = new SMSService();
    this.userService = new UserService(this);
    this.sessionService = new SessionService(this);
  }

  public async hashString(string: string): Promise<string> {
    return await bcrypt.hash(string, environment.security.saltRounds);
  }

  public generateTemporaryCode(length: number = 64, numbersOnly: boolean = false): string | number {
    let token;
    if (numbersOnly) {
      token = Math.round(Math.random()*length);
    } else {
      token = "";
      while (token.length < length) {
        token += Math.random().toString(16).substring(2, 10);
      }
    }
    return token;
  }

  // public async checkPasswordReset(resetCode: string): Promise<SecurityResponse> {
  //   const users = await this.prisma.user.findMany({
  //     where: {
  //       activation: ActivationStatus.PASSWORD_RESET,
  //       statusNote: resetCode
  //     }
  //   })
  //
  //   // Check if user with email exists
  //   if (users.length < 1) {
  //     return {
  //       status: 400,
  //       securityStatus: SecurityCode.INVALID_CODE,
  //       message: "Unable to reset password",
  //       error: "Invalid reset code"
  //     }
  //   }
  //
  //   // Check if multiple users have the same email
  //   if (users.length > 1) {
  //     return {
  //       status: 409,
  //       securityStatus: SecurityCode.CONFLICT,
  //       message: "Unable to reset password",
  //       error: "Multiple users with the same reset code found"
  //     }
  //   }
  //
  //   // Allow password reset if only one user is found
  //   const user = users[0];
  //   return {
  //     status: 200,
  //     securityStatus: SecurityCode.SUCCESS,
  //     message: "Password reset allowed",
  //     data: {
  //       id: user.id,
  //       username: user.username,
  //       resetCode: user.statusNote
  //     }
  //   }
  // }

  // public async resetPassword(resetCode: string, userId: number, newPassword: string): Promise<SecurityResponse> {
  //   const user = await this.prisma.user.findFirst({
  //     where: {
  //       id: userId,
  //       activation: ActivationStatus.PASSWORD_RESET,
  //       statusNote: resetCode
  //     }
  //   })
  //
  //   // Check if user with email exists
  //   if (!user) {
  //     return {
  //       status: 400,
  //       securityStatus: SecurityCode.INVALID_CODE,
  //       message: "Unable to reset password",
  //       error: `No user with pending activation and matching resetCode found for id ${userId}`
  //     }
  //   }
  //
  //   // Check if password is new
  //   if (await bcrypt.compare(newPassword, user.passwordHash)) {
  //     return {
  //       status: 400,
  //       securityStatus: SecurityCode.INVALID_PASSWD,
  //       message: "Unable to reset password",
  //       error: "New password cannot be the same as current password"
  //     }
  //   }
  //
  //   // Change password
  //   await this.prisma.user.update({
  //     where: {
  //       id: userId
  //     },
  //     data: {
  //       activation: ActivationStatus.ACTIVE,
  //       statusNote: "",
  //       passwordHash: await this.hashString(newPassword)
  //     }
  //   });
  //   const mailParams: MailParams = {
  //     receivers: user.email,
  //     subject: "Password reset",
  //     message: `Hey ${user.username}! Your password has been reset`
  //   }
  //   await this.mailService.sendMail(mailParams);
  //   return {
  //     status: 200,
  //     securityStatus: SecurityCode.SUCCESS,
  //     message: "Password reset successful"
  //   }
  // }

  public async doLogin(ip: string, loginDTO: UserLoginDTO): Promise<SecurityDTO> {
    this.log.debug(`Performing login for ${loginDTO.username}`);

    const authData: AuthData = await this.authenticate(loginDTO.username, loginDTO.password);

    if (!authData.user) {
      this.log.error(`Error while performing login`, authData.error);
      throw new ApiBaseError(authData.error?.status || 500, authData.error?.message || 'Internal Server Error');
    }

    let session: Session = await this.sessionService.getSessionForUserAndIpHash(authData.user, ip);
    if (loginDTO.session && session.id !== loginDTO.session) {
      return {
        status: 401,
        message: 'Invalid Session',
        authType: AuthType.INVALID_SESSION
      }
    }
    if (authData.authType == AuthType.PENDING_MFA && session.status == SessionStatus.PENDING_MFA) {
      if (loginDTO.otp) {
        if (authData.user.multiFactorAuth === MFAType.TOTP && authData.user.totpSecret) {
          if (this.checkTOTP(authData.user.totpSecret, loginDTO.otp)) {
            session.status = SessionStatus.ACTIVE;
            return {
              status: 200,
              message: 'Login successful',
              session: session.id,
              authType: AuthType.SUCCESS
            }
          } else {
            return {
              status: 401,
              message: 'Invalid TOTP',
              authType: AuthType.INVALID_MFA
            }
          }
        } else if (authData.user.multiFactorAuth === MFAType.SMS || authData.user.multiFactorAuth === MFAType.EMAIL) {
          if (
            authData.user.oneTimeCodeCode &&
            await this.isOTPValid(authData.user.oneTimeCodeCode) &&
            authData.user.oneTimeCodeCode === loginDTO.otp
          ) {
            session.status = SessionStatus.ACTIVE;
            return {
              status: 200,
              message: 'Login successful',
              session: session.id,
              authType: AuthType.SUCCESS
            }
          } else {
            return {
              status: 401,
              message: 'Invalid OTP',
              authType: AuthType.INVALID_MFA
            }
          }
        } else {
          return {
            status: 500,
            message: 'Internal Server Error',
            authType: AuthType.ERROR
          }
        }
      } else {
        return {
          status: 401,
          message: 'Missing OTP',
          session: session.id,
          authType: AuthType.INVALID_MFA
        }
      }
    } else {
      if (authData.authType === AuthType.SUCCESS && session.status === SessionStatus.STATUS_EVAL) {
        if (authData.user.multiFactorAuth === MFAType.NONE) {
          session.status = SessionStatus.ACTIVE;
          return {
            status: 200,
            message: 'Login successful',
            session: session.id,
            authType: AuthType.SUCCESS
          }
        } else {
          session.status = SessionStatus.PENDING_MFA;
          const otp = await this.generateOTP();
          if (authData.user.multiFactorAuth === MFAType.SMS && authData.user.phone) {
            await this.smsService.sendSMS(`Hey ${authData.user.username}! Here's your one time code for the Inventory Manager: ${otp.code}`, authData.user.phone);
          }
          if (authData.user.multiFactorAuth === MFAType.EMAIL) {
            const mailParams: MailParams = {
              receivers: authData.user.email,
              message: `Hey ${authData.user.username}! Here's your one time code for the Inventory Manager: ${otp.code}`,
              subject: 'Your OneTimeCode for Inventory Manager'
            }
            await this.mailService.sendMail(mailParams);
          }
          return {
            status: 401,
            message: "Pending MFA",
            session: session.id,
            authType: AuthType.PENDING_MFA
          }
        }
      } else {
        return {
          status: 401,
          message: 'Invalid Credentials',
          authType: AuthType.INVALID_CREDENTIALS
        }
      }
    }
  }

  public generateTOTPToken(): string {
    return speakeasy.generateSecret(environment.security.totpOptions).base32;
  }

  public checkTOTP(secret: string, totp: number): boolean {
    return speakeasy.totp.verify({
      secret: secret,
      encoding: 'base32',
      token: totp
    });
  }

  public async getRealIpHash(req: Request): Promise<string> {
    let clientIp = req.header('X-Forwarded-For');
    if (!clientIp || clientIp === "") {
      clientIp = req.header('X-Real-Ip');
    }
    if (!clientIp || clientIp === "") {
      clientIp = req.ip;
    }
    return await this.hashString(clientIp);
  }

  private async isOTPValid(code: number): Promise<boolean> {
    const otp: OneTimeCode | null = await this.prisma.oneTimeCode.findFirst({
      where: {
        code: code,
      }
    })

    const now: Date = new Date();
    return !(otp === null || now >= otp.invalidBy);
  }

  private async generateOTP(): Promise<OneTimeCode> {
    const code: number = <number>this.generateTemporaryCode(6, true);
    const date = new Date();
    const invalidBy = new Date(date.setDate(date.getMinutes() + environment.security.otpOptions.validForXMin));
    return await this.prisma.oneTimeCode.create({
      data: {
        code: code,
        invalidBy: invalidBy,
      }
    });
  }

  private async authenticate(username: string, password: string): Promise<AuthData> {
    this.log.debug(`Authenticating user ${username}`);
    // Get user
    const userServiceResponse: UserServiceResponse = await this.userService.getUserByUsername(username);

    if (userServiceResponse.status !== 200) {
      this.log.error('Error in UserService. Unable to proceed with authentication', userServiceResponse.message);
      return {
        authType: AuthType.ERROR,
        error: {
          status: userServiceResponse.status,
          message: 'Invalid Status'
        }
      }
    }

    const user: User = userServiceResponse.data;

    // Check if user exists
    if (!user) {
      return {
        authType: AuthType.ERROR,
        error: {
          status: 404,
          message: 'Not Found'
        }
      }
    }

    // Check if user is blocked
    if (user.activation === ActivationStatus.BLOCKED) {
      return {
        authType: AuthType.INACTIVE_USER,
        error: {
          status: 403,
          message: 'Blocked'
        },
        user: user
      }
    }

    // Check if user has a pending verification
    if (user.activation === ActivationStatus.PENDING) {
      return {
        authType: AuthType.INACTIVE_USER,
        error: {
          status: 403,
          message: 'Activation Pending'
        },
        user: user
      }
    }

    // Check if user has a pending password reset
    if (user.activation === ActivationStatus.PASSWORD_RESET) {
      return {
        authType: AuthType.INACTIVE_USER,
        error: {
          status: 403,
          message: 'Pending Password Reset'
        },
        user: user
      }
    }

    // Compare password with database hash
    if (await bcrypt.compare(password, user.passwordHash)) {
      // Check if user has 2fa enabled
      if (user.multiFactorAuth === MFAType.NONE) {
        return {
          authType: AuthType.SUCCESS,
          user: user
        }
      } else {
        return {
          authType: AuthType.PENDING_MFA,
          user: user
        }
      }
    } else {
      return {
        authType: AuthType.INVALID_CREDENTIALS,
        error: {
          status: 401,
          message: 'Invalid Credentials'
        },
        user: user
      }
    }
  }
}
