import twilio, {Twilio} from "twilio";
import {environment} from "../environment/environment";
import {MessageInstance} from "twilio/lib/rest/api/v2010/account/message";
import {Logger} from "../helpers/Logger";

export class SMSService {
  log: Logger;
  client: Twilio;

  constructor() {
    this.log = new Logger('SMSService');
    this.client = twilio(environment.sms.account, environment.sms.token);
  }

  public async sendSMS(message: string, receiver: string): Promise<MessageInstance | null> {
    if (environment.sms.enabled) {
      const response = await this.client.messages.create({
        body: message,
        from: environment.sms.sender,
        to: receiver
      });
      return response;
    } else {
      this.log.info(`Sending SMS is disabled. Intended to send ${message} to ${receiver}`)
    }
    return null;
  }
}