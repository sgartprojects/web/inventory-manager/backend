import {Logger} from "../helpers/Logger";
import {ActivationStatus} from "../enums/user.enums";
import {EmailService} from "./email.service";
import {SecurityService} from "./security.service";
import {PrismaClient, User} from "@prisma/client";
import {environment} from "../environment/environment";
import {InvalidArgumentError} from "../errors/invalidargumentError";
import {UserRegistrationDTO} from "../dto/incoming/user.dto";
import {UserServiceResponse} from "../responses/user.response";
import {MFAType} from "../enums/security.enums";
import {MailParams} from "../interfaces/email.models";

export class UserService {
  private readonly mailService: EmailService;
  private readonly securityService: SecurityService;
  private readonly log: Logger;
  private readonly prisma: PrismaClient;
  constructor(securityService: SecurityService | undefined = undefined ) {
    this.mailService = new EmailService();
    this.log = new Logger("UserService");
    this.prisma = new PrismaClient();
    if (securityService) {
      this.securityService = securityService
    } else {
      this.securityService = new SecurityService();
    }
  }

  public async registerUser(userRegistration: UserRegistrationDTO): Promise<UserServiceResponse> {
    if (await this.usernameTaken(userRegistration.username)) {
      this.log.warn(`Skipped creation of user with username ${userRegistration.username} as a user with this name already exists`);
      return {
        status: 400,
        message: "Username already taken",
        error: "Username already taken"
      }
    }
    const activationCode:string = <string>this.securityService.generateTemporaryCode()

    const user: User = await this.prisma.user.create({
      data: {
        username: userRegistration.username,
        email: userRegistration.email,
        passwordHash: await this.securityService.hashString(userRegistration.password),
        activation: ActivationStatus.PENDING,
        statusNote: activationCode,
        multiFactorAuth: MFAType.NONE,
        creationDate: new Date(),
        groups: {},
        sessions: {},
        languageIsoCode: "de",
        theme: "Default"
      }
    }).catch((err) => {
      throw new InvalidArgumentError(
        "Invalid user data was provided for account creation",
        400,
        err,
        {
          name: "user data",
          description: "username, email or password"
        }
      )
    });

    const mailParams: MailParams = {
      subject: "User Registration",
      receivers: userRegistration.email,
      message: `<h2>Hey ${user.username}!</h2><br>Please click the following link to complete your registration: 
      <a href="${environment.server.url}/register/confirm/${user.statusNote}">Complete registration</a>`
    }

    await this.mailService.sendMail(mailParams);
    return {
      status: 201,
      message: `User created`
    }
  }

  public async approveUser(approvalCode: string): Promise<UserServiceResponse> {
    const users = await this.prisma.user.findMany({
      where: {
        activation: ActivationStatus.PENDING,
        statusNote: approvalCode
      }
    })
    if (users.length > 1) {
      this.log.error("Error while setting approval status", "Multiple users with the same approval code found");
      return {
        status: 409,
        message: "Conflicting approval code",
        error: "Multiple users with the same approval code found"
      }
    }
    if (users.length < 1) {
      this.log.debug("Invalid approval code");
      return {
        status: 400,
        message: "Invalid approval code"
      }
    }

    const user = users[0];
    await this.prisma.user.update({
      where: {
        id: user.id
      },
      data: {
        activation: ActivationStatus.ACTIVE,
        statusNote: ""
      }
    })

    const mailParams: MailParams = {
      receivers: user.email,
      subject: "You're good to go",
      message: "Your account has been confirmed. You can now login at xyz"
    }
    await this.mailService.sendMail(mailParams);
    return {
      status: 200,
      message: "User activated"
    }
  }

  public async requestPasswordReset(email: string): Promise<UserServiceResponse> {
    // Get all users with email
    const users = await this.prisma.user.findMany({
      where: {
        email: email
      }
    })

    // Check if user with email exists
    if (users.length < 1) {
      return {
        status: 404,
        message: "Unable to reset password for this email",
        error: `Email ${email} is not linked to any accounts`
      }
    }

    // Check if multiple users have the same email
    if (users.length > 1) {
      return {
        status: 409,
        message: "Unable to reset password for this email",
        error: `Multiple users with email ${email} found`
      }
    }

    // Reset password if only one user is found
    const user = users[0];
    const resetCode: string = <string>this.securityService.generateTemporaryCode();
    await this.prisma.user.update({
      where: {
        id: user.id
      },
      data: {
        activation: ActivationStatus.PASSWORD_RESET,
        statusNote: resetCode
      }
    });
    const mailParams: MailParams = {
      receivers: user.email,
      subject: "Password reset",
      message: `A password reset was requested for account ${user.username}.<br>Click <a href=${environment.server.url}/login/reset/${resetCode}>here</a> to reset your password`
    }
    await this.mailService.sendMail(mailParams);
    return {
      status: 200,
      message: "Reset email sent to user"
    }
  }

  public async enable2FA(userId: number, type: MFAType): Promise<UserServiceResponse> {
    if (type === MFAType.NONE) {
      return {
        status: 400,
        message: "Unable to enable 2FA",
        error: "Type can't be none"
      }
    }
    let totpSecret: string | undefined = undefined;
    let otp: string | undefined = undefined;
    if (type === MFAType.TOTP) {
      totpSecret = this.securityService.generateTOTPToken();
    }
    const user = await this.prisma.user.findFirst({
      where: {
        id: userId
      }
    });
    if (!user) {
      return {
        status: 404,
        message: "Unable to enable 2FA",
        error: `User with ID ${userId} does not exist`
      }
    }
    await this.prisma.user.update({
      where: {
        id: user.id
      },
      data: {
        totpSecret: totpSecret,
        multiFactorAuth: type,
        oneTimeCodeCode: otp
      }
    });
    const mailParams: MailParams = {
      receivers: user.email,
      subject: "TOTP enabled",
      message: "TOTP has been successfully enabled on your account"
    }
    await this.mailService.sendMail(mailParams);
    return {
      status: 200,
      message: "TOTP enabled successfully"
    }
  }

  public async getUserByUsername(username: string): Promise<UserServiceResponse> {
    const users: User[] = await this.prisma.user.findMany({
      where: {
        username: username
      }
    });

    if (users.length > 1) {
      return {
        status: 500,
        message: "Internal Server Error",
        error: "Multiple users with username found"
      };
    }
    if (users.length == 0) {
      return {
        status: 400,
        message: "Bad Argument",
        error: "No user with given username found"
      }
    }
    return {
      status: 200,
      message: "Success",
      data: users[0]
    }
  }

  public async usernameTaken(username: string): Promise<boolean> {
    const results = await this.prisma.user.findMany({
      where: {
        username: username
      }
    })
    return results.length > 0;
  }
}
