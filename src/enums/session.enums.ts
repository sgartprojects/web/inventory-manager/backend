export enum SessionStatus {
  ACTIVE = 'ACTIVE',
  PENDING_MFA = 'PENDING_MFA',
  STATUS_EVAL = 'STATUS_EVAL'
}