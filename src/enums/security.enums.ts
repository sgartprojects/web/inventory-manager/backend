export enum MFAType {
  NONE = "None",
  TOTP = "TOTP",
  SMS = "SMS",
  EMAIL = "Email"
}

export enum AuthType {
  SUCCESS = 'SUCCESS',
  PENDING_MFA = 'PENDING_MFA',
  INVALID_MFA = 'INVALID_MFA',
  INVALID_CREDENTIALS = 'INVALID_CREDENTIALS',
  INVALID_SESSION = 'INVALID_SESSION',
  INACTIVE_USER = 'INACTIVE_USER',
  ERROR = 'ERROR'
}