export enum ActivationStatus {
  ACTIVE = 'Active',
  BLOCKED = 'Blocked',
  PENDING = 'Pending',
  PASSWORD_RESET = "Password_Reset"
}
