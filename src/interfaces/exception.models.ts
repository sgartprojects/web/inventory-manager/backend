export interface BasicField {
  name: string,
  description?: string
}