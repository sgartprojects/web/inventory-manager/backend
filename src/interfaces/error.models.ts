export interface BaseError {
  status: number,
  message: string,
  error?: string | object
}