import {Response} from "express";

export interface MailParams {
  receivers: string;
  subject: string;
  template?: {
    template: string;
    params: TemplateParams[];
  }
  message?: string;
}

export interface TemplateParams {
  key: string;
  value: string;
}

export interface MailServiceResponse {
  status: number;
  message: string;
  details?: string;
}