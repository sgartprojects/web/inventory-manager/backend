export interface Group {
  id: string;
  name: string;
  permissions: Permission[];
}

export interface Permission {
  id: string;
  admin: boolean;
  moderator: boolean;
  member: boolean;
  guest: boolean;
}