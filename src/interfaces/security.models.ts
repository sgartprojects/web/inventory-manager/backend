import {AuthType} from "../enums/security.enums";
import {User} from "@prisma/client";
import {BaseError} from "./error.models";

export interface AuthData {
  authType: AuthType,
  user?: User,
  error?: BaseError;
}