import {BasicDTO} from "./basic.dto";
import {AuthType} from "../../enums/security.enums";

export interface SecurityDTO extends BasicDTO {
  authType: AuthType;
  session?: string;
}