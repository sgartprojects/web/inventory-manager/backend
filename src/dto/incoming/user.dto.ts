export interface BasicUserAuthenticationDTO {
  username: string;
  password: string;
}

export interface UserRegistrationDTO extends BasicUserAuthenticationDTO {
  email: string;
  phone: string | undefined;
}

export interface UserLoginDTO extends BasicUserAuthenticationDTO {
  otp: number | undefined;
  session: string | undefined;
}