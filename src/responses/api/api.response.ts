import {BasicResponse} from "../internal/basic.response";

export class ApiResponse extends BasicResponse {
  message: string;
  constructor(
    code: number,
    message: string,
    data?: object
  ) {
    super(data);
    this.message = message
  }
}