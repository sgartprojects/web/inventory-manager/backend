import {AuthType} from "../../enums/security.enums";
import {ApiResponse} from "./api.response";

export class SecurityResponse extends ApiResponse {
  authType: AuthType;

  constructor(
    status: number,
    authType: AuthType,
    message: string,
    data?: object
  ) {
    super(status, message);
    this.authType = authType;
    this.data = data;
  }
}