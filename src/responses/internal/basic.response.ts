export class BasicResponse {
  data?: object;

  constructor(
    data?: object
  ) {
    this.data = data;
  }
}