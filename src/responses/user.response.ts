export interface UserServiceResponse {
  status: number;
  message: string;
  error?: string;
  data?: any;
}