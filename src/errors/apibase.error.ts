import {environment} from "../environment/environment";

export class ApiBaseError extends Error {
  public status: number;
  public error?: string | object;

  constructor(status: number, message: string, error?: string | object) {
    super(message);
    this.status = status;
    this.error = error;
    if (environment.server.errors.disableStacktrace) {
      super.stack = undefined;
    }
  }
}