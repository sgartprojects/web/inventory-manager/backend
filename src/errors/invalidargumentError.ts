import {ApiBaseError} from "./apibase.error";
import {BasicField} from "../interfaces/exception.models";

export class InvalidArgumentError extends ApiBaseError {
  public invalidArgument: BasicField

  constructor(
    message: string,
    status: number,
    error: string | object,
    invalidArgument: BasicField) {
    super(status, message, error);
    this.invalidArgument = invalidArgument;
  }
}