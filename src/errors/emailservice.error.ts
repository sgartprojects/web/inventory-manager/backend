import {ApiBaseError} from "./apibase.error";

export class EmailServiceError extends ApiBaseError{
  constructor(
    message: string,
    status: number,
    error: string | object) {
    super(message, status, error);
  }
}