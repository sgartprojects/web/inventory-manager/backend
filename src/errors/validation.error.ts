import {ApiBaseError} from "./apibase.error";

export class ValidationError extends ApiBaseError {
  field: string;

  constructor(
    field: string,
    error: string) {
    super(
      400,
      `Failed to validate field "${field}" with code 400: ${error}`,
      error);
    this.field = field;
  }
}