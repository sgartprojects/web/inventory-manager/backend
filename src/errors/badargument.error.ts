import {BasicField} from "../models/exception.models";
import {ApiBaseError} from "./apibase.error";

export class BadArgumentError extends ApiBaseError {
  public badArgument: string | BasicField

  constructor(
    status: number,
    message: string,
    badArgument: string | BasicField,
    error?: string | object,
  ) {
    super(status, message, error);
    this.badArgument = badArgument;
  }
}