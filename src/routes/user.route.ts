import express from "express";
import userController from "../controllers/user.controller";
import securityController from "../controllers/security.controller";

const userRoutes = express.Router()
userRoutes.post("/register/new", userController.createUser);
userRoutes.post("/login", securityController.doLogin);
// userRoutes.post("/login/requestReset", userController.requestPasswordReset);
// userRoutes.post("/login/reset", securityController.resetPassword);
// userRoutes.get("/login/reset/:resetCode", securityController.checkPasswordReset);
userRoutes.get("/register/confirm/:approvalCode", userController.completeRegistration);

// ToDo: The routes below are for testing purposes only and shoudl be removed before release
userRoutes.post("/user/enable2FA", userController.enable2FA);
userRoutes.post("/user/checkTOTP", securityController.checkTOTP);
export default userRoutes;
