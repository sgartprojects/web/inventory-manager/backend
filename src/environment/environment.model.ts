import {LogLevel} from "../enums/log.enums";

export interface Environment {
  mail: {
    enabled: boolean;
    sender: {
      name: string;
      address: string;
    },
    host: string;
    apiKey: string;
    domain: string;
  },
  sms: {
    enabled: boolean;
    sender: string;
    account: string;
    token: string;
  },
  security: {
    saltRounds: number;
    totpOptions: {
      name: string;
    },
    otpOptions: {
      validForXMin: number;
    }
    session: {
      secret: string;
      validForXDays: number;
    };
  },
  server: {
    url: string;
    errors: {
      disableStacktrace: boolean;
    },
    log: {
      logLevel: LogLevel;
    }
  }
}