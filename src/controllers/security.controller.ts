import {SecurityService} from "../services/security.service";
import {Request, Response} from "express";
import {SMSService} from "../services/sms.service";
import {UserLoginDTO} from "../dto/incoming/user.dto";

const securityService: SecurityService = new SecurityService();
const smsService: SMSService = new SMSService();

const doLogin = async (req: Request, res: Response) => {
  if (!req.body.user) {
    res.status(400).json({
      message: "Missing parameter",
      error: "Missing parameter"
    })
    return;
  }
  let loginDTO: UserLoginDTO = req.body.user;
  loginDTO.session = req.header('Session');
  const clientIp = await securityService.getRealIpHash(req);
  const response = await securityService.doLogin(clientIp, req.body.user);
  res.status(response.status).json({
    response
  })
}

// const checkPasswordReset = async (req: Request, res: Response) => {
//   if (!req.params.resetCode) {
//     res.status(400).json({
//       message: "Missing parameter",
//       error: "Missing parameter"
//     })
//     return;
//   }
//   const response = await securityService.checkPasswordReset(req.params.resetCode);
//   res.status(response.status).json({
//     response
//   })
// }

// const resetPassword = async (req: Request, res: Response) => {
//   if (!req.body.resetCode || !req.body.user || !req.body.password) {
//     res.status(400).json({
//       message: "Missing parameter",
//       error: "Missing parameter"
//     })
//     return;
//   }
//   const response = await securityService.resetPassword(req.body.resetCode, req.body.user, req.body.password);
//   res.status(response.status).json({
//     response
//   })
// }

// ToDo: Remove before release. Just for testing!
const checkTOTP = async (req: Request, res: Response) => {
  if (!req.body.token || !req.body.totp) {
    res.status(400).json({
      message: "Missing parameter",
      error: "Missing parameter"
    })
    return;
  }
  const response = securityService.checkTOTP(req.body.token, req.body.totp);
  res.status(200).json({match:response});
}
const sendSMS = async (req: Request, res: Response) => {
  if (!req.body.message || !req.body.receiver) {
    res.status(400).json({
      message: "Missing parameter",
      error: "Missing parameter"
    })
    return;
  }
  const response = smsService.sendSMS(req.body.message, req.body.receiver);
  res.status(200).json(response);
}

export default {doLogin, checkTOTP, sendSMS}