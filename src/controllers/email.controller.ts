import { Request, Response, NextFunction } from 'express';
import {EmailService} from "../services/email.service";
import {MailParams, MailServiceResponse} from "../models/email.models";
const sendTestMail = async (req: Request, res: Response, next: NextFunction) => {
  const mailService: EmailService = new EmailService();
  const mailParams: MailParams = {
    receivers: "dm@sgart.dev",
    subject: "Test",
    message: "This is a test"
  }
  const mailServiceResponse: MailServiceResponse = await mailService.sendMail(mailParams);

  res.status(mailServiceResponse.status)
    .json({
      message: mailServiceResponse.message,
      details: mailServiceResponse.details
    });
}

export default {sendTestMail};