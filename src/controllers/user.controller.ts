import { Request, Response, NextFunction } from 'express';
import {UserService} from "../services/user.service";
import {UserRegistrationValidator} from "../validators/user.validators";
import {MFAType} from "../enums/security.enums";

const userService: UserService = new UserService();
const userRegistrationValidator = new UserRegistrationValidator();

const createUser = async (req: Request, res: Response, next: NextFunction) => {
  userRegistrationValidator.validate(req.body.user)
    .then(() => {
      userService.registerUser(req.body.user)
        .then((data) => {
          res.status(data.status).json(data.data)
        })
        .catch(next);
    })
    .catch(next);
}

const completeRegistration = async (req: Request, res: Response) => {
  if (!req.params.approvalCode) {
    res.status(400).json({
      message: "Missing parameter",
      error: "Missing parameter"
    })
    return;
  }
  const response = await userService.approveUser(req.params.approvalCode);
  res.status(response.status).json({
    message: response.message,
    error: response.error
  })
}

const requestPasswordReset = async (req: Request, res: Response) => {
  if (!req.body.email) {
    res.status(400).json({
      message: "Missing parameter",
      error: "Missing parameter"
    })
    return;
  }
  const response = await userService.requestPasswordReset(req.body.email);
  res.status(response.status).json({
    message: response.message,
    error: response.error
  })
}

// ToDo: This is just for testing - Remove on release
const enable2FA = async (req: Request, res: Response) => {
  if (!req.body.user) {
    res.status(400).json({
      message: "Missing parameter",
      error: "Missing parameter"
    });
    return;
  }
  if (!Object.values(MFAType).includes(req.body.type)) {
    res.status(400).json({
      message: "Invalid parameter",
      error: "Invalid parameter"
    });
    return;
  }
  const response = await userService.enable2FA(req.body.user, req.body.type);
  res.status(response.status).json(response);
}

export default {createUser, completeRegistration, requestPasswordReset, enable2FA};
