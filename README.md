# Inventory Manager  (BE)

### Initial Setup
```shell
# Clone repository
cd $pathToRepository

npm i

npx prisma generate
npx prisma migrate dev --name init

# Start the backend by typing
npm run dev
```

To create a user, two languages have to be manually created first
```shell
npx prisma studio

# Neue Sprachen Deutsch und English erstellen
# de - Deutsch
# en - English
```

The API would now be ready for use, if it was actually completed...

---

### ToDo's
- [ ] Do code cleanup (a lot)
  - [ ] Unnecessairy controllers
  - [ ] Unnecessairy routes
  - [ ] Commented and dead code
  - [ ] etc.
- [ ] Resolve In-Code-ToDo's
- [ ] Complete API
- [ ] Complete Logger Implementation

---

### Documentation
Visit the Repo-Wiki for a Dokumentation

---

### Changelog

**2023-01-17**
- Code dump
- Update README.md